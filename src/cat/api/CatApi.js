

const fetchCats = async (page) => {
    
    const url = 'https://api.thecatapi.com/v1/images/search?limit=8&page=${page}&order=desc';
    const response = await fetch(url);

    return response.json();
};

export { fetchCats };
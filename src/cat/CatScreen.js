import React, { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";
import { fetchCats } from "./api/CatApi";

const CatScreen = () => {

    //state
    const [cats, setCats] = useState([]);
    const [page, setPage] = useState(1);

    //hook
    useEffect(() => {
        const upDateCatValue = async () => {
            setCats(await fetchCats(page));
        };

        upDateCatValue();
    }, [page]);

    

    const goToNextPage = () => {
        setPage(page + 1);
    };

    const goToPreviousPage = () =>{
        setPage(page - 1);
    }

    console.log(cats);

    return(
        <>

        <h1>Chats en vrac</h1>
        
        <div className="app">
        {cats.map((cat) => (
            <img className key={cat.id} src={cat.url} />
        ))}

        </div>

        <ReactPaginate
        previousLabel={'previous'}
        nextLabel={'next'}
        pageCount={6}
        marginPagesDisplayed={6}
        containerClassName={'pagination justify-content-center'} 
        pageClassName={'page-item'}
        pageLinkClassName={'page-link'}
        previousClassName={'page-item'}
        previousLinkClassName={'page-link'}
        nextClassName= {'page-item'}
        nextLinkClassName={'page-link'}
        activeClassName={'active'}
        />

        <button className="btn" onClick={goToPreviousPage} >
             Previous page
        </button>
        <button className="btn" onClick={goToNextPage} >
            Next Page
        </button>
        
    </>
    );
};

export default CatScreen;